// Copyright 2013 The Go Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

// This code is copied (and then modified from golang.org/x/tools/oracle/freevars.go)

package functionextractor

import (
	"go/token"

	"go/ast"
	"go/types"

	"golang.org/x/tools/go/ast/astutil"
	"golang.org/x/tools/go/loader"
)

// refMap maps expressions to their types.
type refMap map[ast.Expr]types.Type

// Returns a list of variables referenced in a section that were not defined in the enclosing scope but not in that section
func refVar(stmt ast.Node, lprog *loader.Program, file *ast.File, startPos, endPos token.Pos, startDefinition, endDefinition *token.Pos) (refMap, error) {

	refsMap := make(refMap)

	//allowErrors(&lconf)

	// TODO
	//lconf.Import("runtime")

	//lconf.Fset.AddFile("", -1, len(src.(string)))

	// if err := importQueryPackage(q.Pos, &lconf); err != nil {
	// 	return err
	// }

	info, _, _ := lprog.PathEnclosingInterval(startPos, endPos)

	fileScope := info.Scopes[file]
	pkgScope := fileScope.Parent()

	// The id and sel functions return non-nil if they denote an
	// object o or selection o.x.y that is referenced by the
	// selection but defined neither within the selection nor at
	// file scope, i.e. it is in the lexical environment.
	var id func(n *ast.Ident) types.Object
	var sel func(n *ast.SelectorExpr) types.Object

	sel = func(n *ast.SelectorExpr) types.Object {
		switch x := astutil.Unparen(n.X).(type) {
		case *ast.SelectorExpr:
			return sel(x)
		case *ast.Ident:
			return id(x)
		}
		return nil
	}

	id = func(n *ast.Ident) types.Object {
		obj := info.Uses[n]
		if obj == nil {
			return nil // not a reference
		}
		if _, ok := obj.(*types.PkgName); ok {
			return nil // imported package
		}
		if !(file.Pos() <= obj.Pos() && obj.Pos() <= file.End()) {
			return nil // not defined in this file
		}
		scope := obj.Parent()
		if scope == nil {
			return nil // e.g. interface method, struct field
		}
		if scope == fileScope || scope == pkgScope {
			return nil // defined at file or package scope
		}
		if startPos <= obj.Pos() && obj.Pos() <= endPos {
			return nil // defined within selection => not free
		}
		if startDefinition != nil && endDefinition != nil &&
			(obj.Pos() < *startDefinition || obj.Pos() > *endDefinition) {
			return nil // defined outside of the area we are looking
		}

		return obj
	}

	ast.Inspect(stmt, func(n ast.Node) bool {
		if n == nil {
			return true // popping DFS stack
		}

		// Is this node contained within the selection?
		// (freevars permits inexact selections,
		// like two stmts in a block.)
		if startPos <= n.Pos() && n.End() <= endPos {
			var obj types.Object
			var prune bool
			switch n := n.(type) {
			case *ast.Ident:
				obj = id(n)

			case *ast.SelectorExpr:
				obj = sel(n)
				prune = true
			}

			if obj != nil {
				expr := n.(ast.Expr)
				typ := info.TypeOf(expr)
				refsMap[expr] = typ

				if prune {
					return false // don't descend
				}
			}
		}

		return true // descend
	})

	return refsMap, nil
}
