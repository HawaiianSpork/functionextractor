// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package functionextractor

import (
	"bytes"
	"go/token"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
)

func assertSameProgram(t *testing.T, expected, result string) {
	resultLines := strings.Split(result, "\n")
	expectedLines := strings.Split(expected, "\n")
	var buffer bytes.Buffer
	err := false
	offset := 0
	started := false

	for i, line := range expectedLines {
		cleanLine := strings.TrimSpace(line)
		if cleanLine == "" && !started {
			offset--
			continue
		}
		started = true
		if i+offset >= len(resultLines) {
			err = true
			buffer.WriteString("++ ")
			buffer.WriteString(cleanLine)
		} else if cleanLine == strings.TrimSpace(resultLines[i+offset]) {
			buffer.WriteString(cleanLine)
		} else {
			err = true
			buffer.WriteString("-- ")
			buffer.WriteString(strings.TrimSpace(resultLines[i+offset]))
			buffer.WriteString("\n++ ")
			buffer.WriteString(cleanLine)
		}
		buffer.WriteString("\n")
	}

	for i := len(expectedLines); i+offset < len(resultLines); i++ {
		buffer.WriteString("-- ")
		buffer.WriteString(strings.TrimSpace(resultLines[i+offset]))
		buffer.WriteString("\n")
		err = true
	}

	if err {
		assert.Fail(t, buffer.String())
	}
}

func TestExtractBlankFunction(t *testing.T) {
	var results bytes.Buffer
	err := extractFromTemplate(`
    package main

    func main() {
			{{start}} {{end}}
      println("Hello")
    }
    `, &results)
	assert.NoError(t, err)
	assertSameProgram(t, `
		package main

    func main() {
      newFunc()

			println("Hello")
    }
		func newFunc() {
		}
		`, results.String())
}

func TestExtractNoArgsNoReturnFunction(t *testing.T) {
	var results bytes.Buffer
	err := extractFromTemplate(`
    package main

    func main() {
      {{start}}println("Hello"){{end}}
    }
    `, &results)
	assert.NoError(t, err)
	assertSameProgram(t, `
		package main

    func main() {
      newFunc()

    }
		func newFunc() {
			println("Hello")
		}
		`, results.String())
}

func TestExtractDeclarationFunction(t *testing.T) {
	var results bytes.Buffer
	err := extractFromTemplate(`
    package main

    func main() {
			{{start}}
			foo := 3
			{{end}}
      println(foo)
    }
    `, &results)
	assert.NoError(t, err)
	assertSameProgram(t, `
		package main

    func main() {

      foo := newFunc()
			println(foo)
    }
		func newFunc() int {
			foo := 3
			return foo
		}
		`, results.String())
}

func TestExtractReDefinitionFunction(t *testing.T) {
	var results bytes.Buffer
	err := extractFromTemplate(`
    package main

    func main() {
			foo := 3
			{{start}}
			foo = 5
			{{end}}
      println(foo)
    }
    `, &results)
	assert.NoError(t, err)
	assertSameProgram(t, `
		package main

    func main() {
			foo := 3
      foo = newFunc()
			println(foo)
    }
		func newFunc() int {
			foo := 5
			return foo
		}
		`, results.String())
}

func TestExtractReDefinitionExpressionFunction(t *testing.T) {
	var results bytes.Buffer
	err := extractFromTemplate(`
    package main

    func main() {
			foo := 3
			foo = {{start}}5
			{{end}}
      println(foo)
    }
    `, &results)
	assert.NoError(t, err)
	assertSameProgram(t, `
		package main

    func main() {
			foo := 3
      foo = newFunc()
			println(foo)
    }
		func newFunc() int {
			foo := 5
			return foo
		}
		`, results.String())
}

func extractFromTemplate(s string, results *bytes.Buffer) error {
	startPos := strings.Index(s, "{{start}}")
	noStart := strings.Replace(s, "{{start}}", "", -1)
	endPos := strings.Index(noStart, "{{end}}")
	nonTemplate := strings.Replace(noStart, "{{end}}", "", -1)
	return ExtractFunctionFromString(nonTemplate, token.Pos(startPos), token.Pos(endPos), results)
}

func TestExtractFunctionWithArgs(t *testing.T) {
	var results bytes.Buffer
	err := extractFromTemplate(`
    package main

    func main() {
			foo := 3
			bob := 5

			{{start}}
      println(foo){{end}}

			println(bob)
    }
    `, &results)
	assert.NoError(t, err)
	assertSameProgram(t, `
		package main

    func main() {
      foo := 3
			bob := 5
			newFunc(foo)

			println(bob)
    }
		func newFunc(foo int) { println(foo) }
		`, results.String())
}

func TestExtractFunctionFromSelector(t *testing.T) {
	var results bytes.Buffer
	// TODO would it be easier to pass the struct?
	err := extractFromTemplate(`
    package main

		type foo struct {
			name string
		}

    func main() {
			bar := foo{"bob"}

			{{start}}
      println(bar.name){{end}}

			println(bar.name)
    }
    `, &results)
	assert.NoError(t, err)
	assertSameProgram(t, `
		package main

		type foo struct {
			name string
		}

    func main() {
			bar := foo{"bob"}
			newFunc(bar.name)

			println(bar.name)
    }
		func newFunc(name string) { println(name) }
		`, results.String())
}

func TestExtractFunctionToSelector(t *testing.T) {
	var results bytes.Buffer
	err := extractFromTemplate(`
    package main

		type foo struct {
			name string
		}

    func main() {
			bar := foo{"bob"}

			{{start}}
			bar.name = "sally"{{end}}

			println(bar.name)
    }
    `, &results)
	assert.NoError(t, err)
	assertSameProgram(t, `
		package main

		type foo struct {
			name string
		}

    func main() {
			bar := foo{"bob"}
			bar.name = newFunc()

			println(bar.name)
    }
		func newFunc() { return "sally" }
		`, results.String())
}

// TODO call on an object
