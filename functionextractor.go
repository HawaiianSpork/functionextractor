// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package functionextractor

import (
	"bytes"
	"fmt"
	"go/ast"
	"go/build"
	"go/format"
	"go/token"

	"golang.org/x/tools/go/loader"
)

// ErrInvalidRanger if endPos is not greater than startPos
var ErrInvalidRange = fmt.Errorf("endPos must be greater than startPos")

// ErrPosOutOfRange if position is outside of file or position is invalid.
type ErrPosOutOfRange struct {
	message string
}

func (e ErrPosOutOfRange) Error() string {
	return e.message
}

func rangeInsideNode(node ast.Node, startPos, endPos token.Pos) (bool, error) {
	if startPos > endPos {
		return false, ErrInvalidRange
	}

	if !node.Pos().IsValid() || !node.End().IsValid() {
		return false, ErrPosOutOfRange{"Position Invalid."}
	}

	return startPos > node.Pos() && endPos < node.End(), nil
}

func findFunc(file *ast.File, startPos, endPos token.Pos) (*ast.FuncDecl, error) {
	for _, decl := range file.Decls {
		switch e := decl.(type) {
		case *ast.FuncDecl:
			if !e.Pos().IsValid() {
				return nil, fmt.Errorf("Position Invalid.")
			}

			if inside, err := rangeInsideNode(e.Body, startPos, endPos); inside || err != nil {
				return e, err
			}
		}
	}

	return nil, fmt.Errorf("Function not found.")
}

func replaceStatements(fun *ast.FuncDecl, program *loader.Program, file *ast.File, startPos, endPos token.Pos, funcName string) (*ast.FuncDecl, error) {
	var newStmts []ast.Stmt

	var firstStmtToExtract int
	var stmt ast.Stmt
	for firstStmtToExtract, stmt = range fun.Body.List {
		if stmt.End() > startPos {
			break
		}
		newStmts = append(newStmts, stmt)
		firstStmtToExtract++
	}

	ref, err := refVar(fun, program, file, startPos, endPos, nil, nil)
	if err != nil {
		return nil, err
	}

	returns, err := refVar(fun, program, file, endPos, fun.End(), &startPos, &endPos)
	if err != nil {
		return nil, err
	}

	var extractedStmts []ast.Stmt
	var lastStmtToExtract int
	for lastStmtToExtract = firstStmtToExtract; lastStmtToExtract < len(fun.Body.List); lastStmtToExtract++ {
		stmt := fun.Body.List[lastStmtToExtract]

		if stmt.Pos() > endPos {
			break
		}
		extractedStmts = append(extractedStmts, stmt)
	}

	funcCall := newFuncCall(funcName, ref, returns)
	newStmts = append(newStmts, funcCall)

	for i := lastStmtToExtract; i < len(fun.Body.List); i++ {
		stmt := fun.Body.List[i]
		newStmts = append(newStmts, stmt)
	}

	fun.Body.List = newStmts

	return newFunction(funcName, extractedStmts, ref, returns), nil
}

func getIdentifier(n ast.Expr) *ast.Ident {
	switch n := n.(type) {
	case *ast.Ident:
		return n

	case *ast.SelectorExpr:
		return n.Sel
	default:
		// TODO handle error cleaner
		panic(fmt.Sprintf("Unknown identifier type %+v", n))
	}
}

func newFunction(name string, stmts []ast.Stmt, argsMap refMap, returns refMap) *ast.FuncDecl {

	parameters := make([]*ast.Field, 0, len(argsMap))
	for ref, typ := range argsMap {
		parameters = append(parameters, &ast.Field{
			Names: []*ast.Ident{getIdentifier(ref)},
			Type:  &ast.Ident{Name: typ.String()}})
	}

	results := make([]*ast.Field, 0, len(returns))
	resultExpr := make([]ast.Expr, 0, len(returns))
	for ref, typ := range returns {
		results = append(results, &ast.Field{
			Type: &ast.Ident{Name: typ.String()}})

		resultExpr = append(resultExpr, getIdentifier(ref))
	}

	if len(returns) > 0 {
		stmts = append(stmts, &ast.ReturnStmt{
			Results: resultExpr,
		})
	}

	return &ast.FuncDecl{
		Doc:  nil,
		Recv: nil,
		Name: &ast.Ident{Name: name},
		Type: &ast.FuncType{Params: &ast.FieldList{List: parameters}, Results: &ast.FieldList{List: results}},
		Body: &ast.BlockStmt{List: stmts},
	}
}

func newFuncCall(name string, argsMap refMap, results refMap) ast.Stmt {
	args := make([]ast.Expr, 0, len(argsMap))
	for expr := range argsMap {
		args = append(args, expr)
	}

	callExpr := ast.CallExpr{
		Fun:  &ast.Ident{Name: name},
		Args: args,
	}

	resultExpr := make([]ast.Expr, 0, len(results))
	for ref := range results {
		resultExpr = append(resultExpr, ref.(*ast.Ident))
	}

	var stmt ast.Stmt
	if len(results) > 0 {
		stmt = &ast.AssignStmt{
			// TODO can always use shorthand?
			Tok: token.DEFINE,
			Lhs: resultExpr,
			Rhs: []ast.Expr{&callExpr},
		}
	} else {
		stmt = &ast.ExprStmt{X: &callExpr}
	}

	return stmt
}

// ExtractFunctionFromString extracts function from string
func ExtractFunctionFromString(content string, startPos, endPos token.Pos, results *bytes.Buffer) error {
	//fileSet := token.NewFileSet()

	build := &build.Default
	lconf := loader.Config{Build: build}

	file, err := lconf.ParseFile("", content)
	if err != nil {
		return err
	}
	lconf.CreateFromFiles("", file)

	program, err := lconf.Load()
	if err != nil {
		return err
	}

	// file, err := parser.ParseFile(fileSet, "", content, parser.AllErrors)
	// if err != nil {
	// 	return err
	// }

	if !file.Pos().IsValid() || !file.End().IsValid() {
		return ErrPosOutOfRange{"Position Invalid In file."}
	}

	if file.Pos() > startPos {
		return ErrPosOutOfRange{fmt.Sprintf("startPos (%d) is less than file start position (%d).", startPos, file.Pos())}
	}

	if file.End() < endPos {
		return ErrPosOutOfRange{fmt.Sprintf("endPos (%d) is greater than file end position (%d).", endPos, file.End())}
	}

	fun, err := findFunc(file, startPos, endPos)

	if err != nil {
		return err
	}

	// TODO: new function name should be unique
	funcName := "newFunc"

	newFunc, err := replaceStatements(fun, program, file, startPos, endPos, funcName)

	if err != nil {
		return err
	}

	file.Decls = append(file.Decls, newFunc)

	if err := format.Node(results, program.Fset, file); err != nil {
		return err
	}

	return nil
}
